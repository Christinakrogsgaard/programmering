# MiniX 5
![](billede.1.png)

![](billede.2.png)

[Link to runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX5/)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX5/sketch.js)


Before I started working on the code itself, I decided on two rules:
1. there should be lines coming from the center in a random direction
2. the lines must be of a random size/length

After determining my rules, I tried to draw my idea down on a piece of paper. My hope was that it would have an effect as a star thrower. The more lines that come, the more star effect that comes. When the program has been running for a long time, however, it gets a square shape, which I have wondered about. But I think I managed to achieve my desired goal, as well as to comply with the rules I had set before.

To make this generative program I have played a bit with the random syntax to make the line appear in different lengths.

It was very effective and instructive for me to lay down some rules before starting to work on the code and then having to find a solution. It was a fun and challenging task. it felt really good to see the running program look like the pattern I had drawn down before I started coding. It was challenging for me to use a for-loop / while-loop but I got one. It does not do much, but I got to use one and got the opportunity to work with it.

In this week's text by Soon, Winnie & Cox, Geoff, we were introduced to generative art. it is defined as follows:

“Generative art refers to any art practice where [sic] artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.” (Philip Galanter, “What is Generative Art? Complexity Theory as a Context for Art Theory,)

That said is generative art when an artist uses a system such as rules. Before I started working on my MiniX, I find generative art confusing and had a hard time understanding how simple a few rules can become an art. It was fun trying to do something creative with a specific process.
This week we were introduced to a project called 10 Print, where the focus is on instructions and randomness. However, I think you can discuss how much there is to randomness when it is someone who sets the rules and boundaries. Thus, there is only talk of randomness to a certain degree. This also applied to my own MiniX, as I could also go back and adjust my rules until I was fairly happy with the result.

## Reference list:

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020
