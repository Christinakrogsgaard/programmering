## MiniX 1
![](Billede1.png)
![](Billede2.png)

[Link to rumne](https://Christinakrogsgaard.gitlab.io/programmering/MiniX1)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX1/sketch.js)


I have made a house that shows how it looks in the daytime, but when you click on the mouse pad it turns into nighttime. I would have liked to make some stars, that would have shown in the nighttime, but I couldn’t figure out how to only make them appear when I click on the mouse pad.

I have been doing this coding primarily using figures. In addition, I have used the function "print (mouseX, mouseY);", to find the exact coordinates on which my figures should be. I have also used the function "function mousePressed () {", to make the change from day to nighttime.

I don’t have any experience in programming and coding, so it has been a big challenge for me to make this miniX1. I think it has been a fun and challenging task despite my lack of knowledge in the field. I had many fun ideas that I would like to have executed, but it has not been possible yet due to my lack of knowledge. I hope to be able to learn even more and then try to get started with some of the ideas in my miniX2 or miniX3.

My program is not that special, which is because it is my first experience with coding. I wish I had some more movement into my coding, but it turned out to be a difficult task. I had a desire to make smoke come out of the chimney. I had looked at a lot of examples that I wanted to include, but since I did not understand what it meant, I chose to leave it out so I would be able to stand by 100% of what I have made.

I tried to read some of the examples on P5js.org to get some inspiration. I think you can learn a lot from reading and trying other people’s codes. You can get many good ideas and get inspiration for some new fun ideas. It has helped me a lot to see and read examples for me to get start-ed on my code as it was my first time to try coding. However, I find it a bit difficult to read other codes if you don’t have any knowledge about what they mean. I think is best to try it yourself and then try out how the codes work and how you can combine them in your work.

You use reading and writing to be able to navigate around the world, have conversations, and have a deeper understanding of society. You could argue that one should also understand coding and software as it is found everywhere in our society. People should perhaps know how to code but without the purpose of becoming professional programmers. Coding is its very own language, but not many people can read or understand it even though it is everywhere in society. I think the difference between learning coding and reading/writing is the lack of motivation to learn to code. It is found everywhere, but it is not a necessity to be able to code to navigate around the community, but it would provide a deeper and better understanding and therefore should people perhaps learn it anyway.

I think it has been exciting to get a greater knowledge in the field. The text has helped me gain a greater knowledge of programming and software in general. They have set in motion some thoughts that I have not thought of before and I look forward to gaining an even better under-standing of programming.
