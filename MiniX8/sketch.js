let data;
let world; //Referer til datasæt med vores egne 1. world problems
let u; // referer til datasæt med statements om Ukraine
let blink = 0

function preload() {
  data = loadJSON("problems.json") //Til at loade din JSON fil
  b = loadImage("u.jpeg") //Baggrundsbillede
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(2)
}

function draw() {
  background(b);
  world= data.WorldP; //WorldP er datasættet fra JSON
  u = data.Ukraine; //Ukraine er datasættet fra JSON

  let i = floor(random(0,21)) //vælger et tilfældigt statement fra arrayet med First World problems
  let j = floor(random(0,16)) //vælger et tilfældigt statement fra arrayet med ukraine

  let statement = world[i] //Bruges sådan man kan sætte teksten ind. World[i] referer til world arrayet der er random mellem 0 - 21
  let statement2 = u[j] //Bruges sådan man kan sætte teksten ind. U[i] referer til world arrayet der er random mellem 0 - 16

// Alle boxene
  noStroke();
  fill(255,180);
  rect(80,35,1800,800);
  fill(0,180);
  rect(110,133,500,30);
  rect(110,433,500,30);

// Alt tekst
  push();
  fill(255,0,0);
  textFont("Courier New");
  textSize(40);
  textStyle(BOLD);
  text("It's horrible when...", 110,150);
  text(statement, 390, 230)
  text("But at least you...", 110,450);
  text(statement2, 390, 530 );
  pop();

  push();
  blink += 1; // Blink stiger med 1 hver gang framerate
  if(blink % 6 <= 4){ //efter tallet er divideret 6, hvis den resterende(remainder) sum er mindre eller lig med 4 er teksten rød, ellers er skriften hvid.
  fill(255,0,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  } else {
  fill(255,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  }
}
