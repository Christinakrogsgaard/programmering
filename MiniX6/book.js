class Book {

  constructor() {
    this.r = 75;
    this.x = width - this.r; //placement on the x-axis
    this.y = random(0,height); //placement on the y-axis
    this.speed = floor(random(1,5 ));
  }

  move() {
    this.x -= 15;
    this.x -= this.speed
  }

  show() {
    image(boImg, this.x, this.y, this.r, this.r);
  }
}
