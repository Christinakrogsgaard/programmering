class Girl {
  constructor() {
  this.r = 250; // størrelsen på imaget
   this.x = 50;
   this.y = height - this.r;
   this.vy = 0;
   this.gravity = 3; // gør sådan hun falder ned til bunden af canvas igen
  }
  jump() {
      this.vy = -30; // Siger hvor højt hun hopper
}

  hits(book) {
    return collideRectRect(this.x, this.y, this.r, this.r, book.x, book.y, book.r, book.r);
  }

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
    this.y = constrain(this.y, 0, height - this.r);
  }

  show() {
    image(gImg, this.x, this.y, this.r, this.r);
  }
}
