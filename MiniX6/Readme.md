# MiniX 6
![](Billede2.png)

![](Billede3.png)

![](Video.mov)
[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX6/sketch.js)

[Link to my runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX6/)

[Link to video of the game](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX6/Video.mov) 

This week's MiniX has been very difficult for me to make. I have had many challenges. I have spent many hours getting acquainted with object-oriented programming (OOP) and working with my code, but I still had a lot of trouble figuring out where I had made mistakes. Fortunately, after many hours of work, I got a result I could see myself fairly satisfied with. However, I still have a small problem I could not fix. I have not been able to make it so that the books actually hit the girl before it says game over.

## Inspiration:
My inspiration for this task was primarily a video by Daniel Shiffman called "Chrome Dinosaur Game)". I would use this video as a help tool but put my imprint on it. In addition to the coding train videos, I have also been inspired by various previous MiniX assignments made by former students.

## Description of my program:
As I said, I had a lot of problems with this MiniX. I had a hard time getting started with it due to a lack of creativity and motivation. Precisely this ended up being the motivation for my game. It works like the classic Chrome dinosaur game, but instead, you control a girl. This girl symbolizes me and my challenges with this game. The girl faces some obstacles in the form of a stack of books and she must jump over these books to complete the game. If she hits the books, she gets a message that she needs to do her homework.

The game consists of two different classes: Book and Girl. The class girl is the main character of the game as it is her you are moving to play the game. The book class is the obstacles of the game. Both of them have their properties such as width and height, and behaviors as show(). I also wanted to make the objects move around on the canvas therefore i used the syntax “move()". I made the books move in a random speed from 1 to 5, to make the game a bit harder. The class Girl also has behaviors such as jump() and hits(). The girl class have has an syntax called "this.gravity = 3" to make the girl fall down to the bottom of the canvas again after the jump.

## Characteristics of OOP and the wider implications of abstraction?

Object-oriented programming (OOP) is characterized by Matthew Fuller and Andrew Goffey in their text "The Obscure Objects of Object Orientation". "Object orientation names an approach to computing that views programs in terms of the interactions between programmatically defined objects – computational objects – rather than as an organized sequence of tasks embodied in a strictly defined ordering of routines and sub-routines.".

OOP is organized even though the objects are abstracted, as the objects are groupings of data; "Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic. The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model." (Soon & Cox,2020)

It also appears in the text "Aesthetic Programming: A Handbook of Software Studies" that OOP provides an understanding of the ways in which objects operate through their relation to other objects.(Soon & Cox,2020)


## A wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
I will admit that at the time I made this game I did not think about the wider cultural context as I made a game based on my own feelings and thoughts at the time. When I think back to my childhood and the games I played, none of the games had any serious discussions or issues they raised. It was primarily all fun and games. But now I know that there are details I may not notice that may seem excluded and that I as a white young woman do not notice it in the same way as some others might do. I made this game as a reflection on my own thoughts and therefore are the object "Girl" and the background  two images I myself have been able to empathize with but in that way I have probably also excluded a lot of other people such as men, people with other housing standards or other skin color. I made the game with the intention that it should be an innocent and socially acceptable game, but when you think about the wider cultural context, it might not be quite as inclusive as I thought.


## Reference list:

Inspiration: [Link](https://thecodingtrain.com/CodingChallenges/147-chrome-dinosaur)

Inspiration: [Link](https://gitlab.com/mathildebg/aesthetic-programming/-/tree/master/miniX7)

Inspiration:[Link](https://gitlab.com/Sofiefm/aesthetic-programming/-/tree/master/miniX7)

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

Matthew Fuller & Andrew Goffey;'The Obscure Objects of Object Orientation'.
