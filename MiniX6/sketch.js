let girl;
let gImg;
let boImg;
let bImg;
let bookArray = [];


function preload() {
  gImg = loadImage('girl.png');
  boImg = loadImage('book.png');
  bImg = loadImage('baggrund.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  girl = new Girl();
}

function keyPressed() {
  if (key == ' ') {
    girl.jump();
  }
}


function draw() {
  if (random(1) < 0.01) {
  bookArray.push(new Book());

}
  background(bImg);
  girl.move();
  girl.show();


  fill(255, 0, 0);
  textSize(20);
  text("PRESS SPACE TO JUMP!", width - 300, 40);

  for (let b of bookArray) {
    b.show();
    b.move();


    if (girl.hits(b)) {
      noLoop();
      textSize(50);
      textAlign(CENTER);
      textStyle(BOLD);
      fill(0);
      text("GAME OVER", width / 2, height / 2);
      textSize(20);
      text("YOU HAVE TO MAKE YOUR HOMEWORK", width / 2, height / 2 + 40);

    }
  }
}
