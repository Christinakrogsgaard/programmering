# MiniX 7
![](Billede1.png)

![](Billede2.png)

![](Billede3.png)

[Link to runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX7/)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX7/sketch.js)

## Which MiniX have you reworked?

In this week's MiniX, I have chosen to look back on the week of data capture. This project is about looking critically at what one accepts before it is too late. It is my perception that users accept conditions and terms without knowing what they are agreeing to. My program shows a website that uses cookies and requires you to click accept. Once you have accepted, you have given up your bank information. The purpose of this project is to be an eye-opener for people on the internet, so they think about what they are agreeing to. I have chosen to change this MiniX as I did not feel I was completely finished with this task. I would have liked to have redesigned my former readme as it was very short and inaccurate. Additionally, I had some issues with a feature I would have liked to have added but was not successful at the time. I would like the button function to only display one text at a time so that both "Thank you, now we have your bank information" and "Sorry you need to accept cookies" could not be displayed at the same time. This turned out to be a complex task for me, but it succeeded in the end.

## What have you changed and why?

I wanted to do so the button function to only display one text at a time so that both texts couldn’t be displayed at the same time. I have also changed my old readme as I felt like it was too short. As a little side note, I have also changed the look of my cookie image. I got some comments that I had not made the cookie image myself, so I have made an improved image in photoshop.

To make the button function work like I wanted it to I had to make some variables and use a conditional statement. I started out making an array with the text I wanted to be shown. Then I made a variable “i” in order to make my if/else if statement work.  I had made two functions to make my text appear. The first function is called “sorryLoad” and the other one is “thankYouLoad”. In order for the program to know which text to use for the selected button, I had set i = 1 and i = 2 in the various functions. Thus, my conditional statement will be called "if i == 1" the text will appear with "Thank you, now we have your bank information". “Else if i == 2” the text is displayed with “Sorry you need to accept cookies”

I have always had a bit of a hard time with conditional statements, so that way it has been very instructive for me to have to put myself more into it. In addition, I have not used Arrays that much before, so it was fun to try to work with them. In hindsight, you can also say I have learned that it pays off to do your readme properly in the first place, so you do not have to go back and change it.

## The relation between aesthetic programming and digital culture?
Last monday we had a guest lecture by Jazmin Morris. She talked about Netiquette, which is the guidelines for communicating online. I think this point fits well with the relationship between aesthetic programming and digital culture. We talked about p5.js' code of conduct and about what it would mean to behave properly online. In p5.js' code of conduct, it says "We are a community of, and in solidarity with, people from every gender identity and expression, sexual orientation, race, ethnicity, language, neuro-type, size, disability, class, religion, culture, subculture, political opinion, age, skill level, occupation, and background ". I have not thought much about the fact that the programs you are coding can be critical and actually affect the people that watch it. So you have to make sure that you are respectful even though you are using your program to come out with your thoughts and opinions.    
