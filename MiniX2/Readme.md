## MiniX 2
![](Billede1.png)
![](Billede2.png)

[Link to my runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX2/)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX2/sketch.js)


I made two emojis. The first emoji is the local Ceres canned beer. I had in a long time thought that needed to be an emoji for a can or bottle of beer. In the design, I have focused on Ceres as I thought it was fun as it is a local beer. I have mainly used different shapes and colors to make this emoji. I have had a lot of focus on the layer as the figures must be in the right order. In addition, I inserted the text "CERES", which was a new feature for me.

My second emoji was supposed to be a woman with a headscarf. When I made it, I did not think there was one in advance, but I have since discovered that Appel already has made a woman with a scarf. My emoji is also made using shapes and different colors. I have made it possible for the woman to changes skin color and eye color. I have only made it, so it goes from a light skin tone to a darker skin tone same as blue to brown eyes. I would have liked it if it could be switched between several color combinations, but I did not succeed whit it this time.

My first thought was that canned beer was a very innocent emoji that no one could get mad at. Denmark has a strong drinking culture of which beer is a completely normal part. However, it may not be everywhere they feel the same. "Modifying the Universal." By Roel Roscam Abbing, Peggy Pierrot and Femke Snelting provide an example of how legislation and different values can be offended by some emojis. "While Russia investigates if it can sue Apple for their representation of sexual diversity…". It is said that Russia is dissatisfied with some of the emojis because they go against their values. About this canned beer, there may be some who would think that it has a bad influence on young people's perception of alcohol to "advertise" with because it is visible. In the USA, you are not allowed to drink alcohol until you are 21 years old, and therefore it may be inappropriate to have it on the phone. In addition, some religions are not allowed to consume alcohol and therefore may be offended by its visibility on the phones.

Concerning the emoji, I have made with a woman with a headscarf, one can criticize that there should be several different skins and eye colors so that more women can relate. To break up with the stereotypes a bit I intentionally made the woman with the light skin first. To challenge the stereotypes, even more, I would have liked to make an emoji with a bit more Asian characteristics with a scarf, but I did not succeed with it. In addition, it would have been fun to be able to change the colors of the clothes, as women do not have to wear red or purple clothes but can also use the so-called "boys colors".
