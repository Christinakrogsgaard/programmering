let r = 240;
let g = 218;
let b = 155;
let r1 = 46;
let g1 = 95;
let b1 = 161;

function setup() {
  createCanvas(500,800);
  background(255);
  noStroke();
}

function draw() {

fill(83,118,53);
rect(130,200,100,160);
rect(140,190,80,15);
triangle(130, 200, 140, 200, 140,190);
triangle(230,200,210,200,220,190);
//den grønne del af dåsen

fill(192)
rect(140,185,80,5)
rect(140,360,80,5)
triangle(130,360,140,360,140,365)
triangle(220,360,230,360,220,365)
rect(138,365,83,3)
//alt det grå på dåsens form

fill(168,246,90)
rect(130,210,100,135)
ellipse(180,275,100,160)

fill(192)
ellipse(180,275,90,130)

fill(83,118,53)
ellipse(180,275,84,124)

fill(168,246,90)
ellipse(179,275,50,70)

fill(193,33,33)
rect(140,263,80,20)
//detaljerne på designet
//Har haft meget fokus på lagene, sådan ovalerne kom i den rigtige rækkefølge

fill(255);
textSize(14);
text('CERES', 155, 278);
//tekstfelt
rect(140,281,80,1)
rect(140,264,80,1)
//skriften og detaljerne ved logoet

fill(173,106,186)
ellipse(400,250,140,150)
rect(330,250,140,55)
ellipse(400,296,140,100)
//tørklædet

fill(64,19,73)
ellipse(400,350,100,130)
rect(350,350,100,70)
//kjolen

fill(r,g,b)
ellipse(400,260,100,100)
//hudfarven


fill(255)
ellipse(375,260,20,25)
ellipse(420,260,20,25)
//øjnene
fill(114,3,3)
ellipse(398,290,30,30)
//mund

fill(r,g,b)
rect(380,273,35,15)
//hudfarven for at danne en mund

fill(r1,g1,b1)
ellipse(375,263,15,15)
ellipse(420,263,15,15)
//øjenfarven

fill(173,106,186)
rect(350,210,100,30)
//triangle(370,230,350,260,350,240)
//triangle(430,230,450,260,460,240)
//del af tørklædet

fill(0)
rect(335,220,130,3)
rect(339,210,120,3)

}

function mousePressed (){
r = 134;
g = 108;
b = 82;
r1 = 60;
g1 = 44;
b1 = 28;
}
