# MiniX 3
![](cookies1.png)
![](cookies2.png)

[Link to runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX4/)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX4/sketch.js)

My project is called cookie-capture because it is based on websites that use cookies. It is my perception that websites use cookies and that users accept them without knowing what they are saying yes to in order to get the column to go away and be able to use the website. I made this art project to expose how websites pressure one to make certain choices without the user knowing what they are doing with it. I wanted to address the seriousness of giving websites access to all your data without having read the terms and conditions.

## Describe your program and what you have used and learned
My program shows a website that uses cookies and requires you to click accept. Once you have accepted, you have given up your bank information. The purpose of this project is to be an eye-opener for people on the internet, so they think about what they are agreeing to.

In this project, I have used a lot of new features. I started by inserting an image using "function preload (){" and "loadImage". Afterward, I made the two buttons using "createButton". I called et buttons; Button1 and Button 2, to make it easier to make the different texts appear. To determine its position and size use "button.position" and "button.size. Then I wanted to make a function when you pressed the button. Therefore I used "button.mousePressed" and made two new functions(){ to make the text fields appear when you click on the different buttons. I would have liked to have made it so that you could only see one of the texts at a time but I couldn't make it work.

## Articulate how your program and thinking address the theme of “capture all.”
My art project addresses the theme "capture all" by forcing the viewer to think about all the risks of not knowing what you are agreeing to. I want to make you think about the terms and conditions before you just accept everything. I wanted to make an exaggerated and slightly provocative project to promote an understanding of the seriousness of accepting things you don't want to.

## What are the cultural implications of data capture?
I have grown up in a highly digitalized world without having any idea how much data was collected around me. I have been one of those who accepted everything just to make the columns go away. I have never before thought about how much access various media actually have to my data which is scary.
This concern is also expressed in the quote by Winnie Soon and Geoff Cox, 2020,  "That even sleep has become datafied seems to point to the extent to which our subjectivities have also been captured. We produce, share, collect, use and misuse, knowingly, or not, massive amounts of data, but what does its capture do to us?". This question very well sums up my concerns about data capture and its impact on our society. I have not thought about the extent of data capture, but it is everywhere at all times of the day. "(..)but suffice to say, for now, that in the era of big data, there appears to be a need to capture data on everything, even from the most mundane actions like button pressing."(Soon & Cox, 2020)

## Reference

Winnie Soon and Geoff Cox, Aesthetic Programming: A Handbook of Software Studies, 2020
