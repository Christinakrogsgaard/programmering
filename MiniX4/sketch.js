let button;
let cookies
function preload(){
cookies=loadImage("cookies.png")
}

function setup (){
//image
  createCanvas (600,600)
  background (255)
imageMode(CENTER)
image(cookies,300,300,370,300)

  //button
 button1 = createButton('Accept');
 button1.style('font-size','18px');
 button1.style("color", "#231608",);
 button1.style("background", "#71d370");
 button1.position(330,510);
 button1.size(90,30);
 button1.mousePressed(thankyouLoad);


 button2 = createButton('Learn more');
 button2.style('font-size','14px');
 button2.style("color", "#231608",);
 button2.style("background", "#d7d1bd");
 button2.position(180, 510);
 button2.size(90,30);
 button2.mousePressed(sorryLoad);

 // Text
  textSize(32);
  fill((35, 22, 8));
  text('This website uses cookies', 120, 480);
}
  function thankyouLoad() {
  fill(0);
  textSize(24);
  textStyle(BOLD);
   text('Thank you, now we have your bank information.', 50, 90);
 }

 function sorryLoad(){
fill(0)
textSize(24);
textStyle(BOLD);
  text ('Sorry you need to accept cookies', 50,120)
}
