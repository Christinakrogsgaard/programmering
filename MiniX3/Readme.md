## MiniX 3
![](Timeglas-billede.png)

[Link to runme](https://christinakrogsgaard.gitlab.io/programmering/MiniX3/)

[Link to my code](https://gitlab.com/Christinakrogsgaard/programmering/-/blob/main/MiniX3/sketch.js)

## What do you want to explore and/or express?

In this minix, I have made a throbber based on the symbolism of an hourglass. It was important to me that the hourglass has some movement to symbolize that time also goes by. I have used rotate (angle) to make the hourglass move 360 degrees. I have used translate to move the 0-point into the middle so I could adjust where the hourglass should rotate. Then I wanted to change the colors inside the hourglass to give an illusion that the sand inside the hourglass is running out. I have used an "if ()" and "}else{" function to make the two triangles change color at a certain time. I would have liked to have done so the triangle changed color at a certain coordinate, but it did not succeed.

## The time related syntaxes

The hourglass symbolizes the passage of time, which I think fits well with a throbber. A throbber is for me a kind of loading icon which is used to show that a computer program is performing an action. I think it is important that the time aspect is a part of it as it refers to the time passing while this action loads and is being implemented. Time is important for human life, which is also described by Hans Lammerant in "How humans and machines negotiate experience of time," in The Techno-Galactic Guide to Software Observation".(2018) He states" The experience of time is an essential element of any form of experience or cognition. Emotions depend to a large extent on expectations, or the potential coming of a future event. Any observing or experience of difference, of presence related to an earlier or later absence, is linked with an experience of time.". This is also related to my whole basis for making an hourglass that can move. Us humans and our emotions depend on expectations, or the potential coming of a future event. We need to be able to see that something is moving so that we know it has not just stalled and become impatient.


## Think about a throbber that you have encountered in digital culture


In this assignment, I have been thinking about Hotmail as I think they are using an interesting throbber. Their throbber is an image of an envelope where a letter comes out. This throbber is well thought out as it fits the purpose of the software, which is to send and receive a digital post. That way, you have a clear idea of what the website is about even before you are using it.

![](Hotmail.png)

## Reference list:

Hans Lammerat, 'How Humans and Maschines Negotiate Experience of Time', 2018.

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020
