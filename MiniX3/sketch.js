//throbber
let angle =0
let timeglas=0
function setup() {
 //create a drawing canvas
 createCanvas(500,500);
 angleMode(DEGREES)
 rectMode(CENTER)
 frameRate(30)
}

function draw () {
  background(255,40);

push();
//timeglas
  translate(width/2,200)
  rotate(angle);
  stroke(0)
  strokeWeight(1)
  timeglas+=1
  if (timeglas%50<=10){
  fill(255)
  triangle(-50,-50,50,-50,0,0);
} else {
  fill(0)
  triangle(-50,-50,50,-50,0,0);
}

if (timeglas%50<=10){
  fill(0)
  triangle(-50,50,50,50,0,0)
} else {
  fill(255)
  triangle(-50,50,50,50,0,0)
}
pop();

angle=angle+1

}
